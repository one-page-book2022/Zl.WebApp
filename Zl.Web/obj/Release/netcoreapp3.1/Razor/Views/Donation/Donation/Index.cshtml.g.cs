#pragma checksum "C:\代码\Zl.WebApp\Zl.Web\Views\Donation\Donation\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5f418590731ffb3db7b1ef3d7b46cad299a174bc"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Donation_Donation_Index), @"mvc.1.0.view", @"/Views/Donation/Donation/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5f418590731ffb3db7b1ef3d7b46cad299a174bc", @"/Views/Donation/Donation/Index.cshtml")]
    public class Views_Donation_Donation_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\代码\Zl.WebApp\Zl.Web\Views\Donation\Donation\Index.cshtml"
  
    ViewBag.Title = "Index";
    Layout = "~/Views/Shared/_LayoutList.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<!--模糊搜索区域-->
<div class=""layui-row"">
    <form class=""layui-form layui-col-md12 ok-search"">
        <div class=""layui-input-inline"">
            <input class=""layui-input"" placeholder=""请输入捐赠人"" name=""Name"" autocomplete=""off"">
        </div>
        ");
#nullable restore
#line 12 "C:\代码\Zl.WebApp\Zl.Web\Views\Donation\Donation\Index.cshtml"
   Write(Html.SearchBtnHtml("查询"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 13 "C:\代码\Zl.WebApp\Zl.Web\Views\Donation\Donation\Index.cshtml"
   Write(Html.ResetBtnHtml());

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    </form>
</div>
<!--数据表格-->
<table class=""layui-hide"" id=""tableId"" lay-filter=""tableFilter""></table>
<script>
    layui.use([""table"", ""form"", ""okLayer"", ""okUtils""], function () {
        let table = layui.table;
        let form = layui.form;
        let okLayer = layui.okLayer;
        let okUtils = layui.okUtils;

        let AllTable = table.render({
            elem: ""#tableId"",
            url: ""/Donation/Donation/List"",
            limit: 10,
            page: true,
            toolbar: ""#toolbarTpl"",
            size: ""sm"",
            cols: [[
                { type: ""checkbox"" },
                { field: ""Id"", title: ""ID"", width: 80, sort: true },
                { field: ""Name"", title: ""捐赠人"" },
                { field: ""Price"", title: ""捐赠金额"" },
                { field: ""Source"", title: ""捐赠方式""},
                { field: ""Detail"", title: ""备注""},
                { field: ""CreateTime"", title: ""创建时间"", width: 150, templet: '<span>{{showDate(d.CreateTime)}}<span>' },
         ");
            WriteLiteral(@"       { title: ""操作"", width: 230, align: ""center"", fixed: ""right"", templet: ""#operationTpl""}
            ]],
            done: function (res, curr, count) {
                console.log(res, curr, count);
            }
        });

        form.on(""submit(search)"", function (data) {
            AllTable.reload({
                where: data.field,
                page: { curr: 1 }
            });
            return false;
        });

        table.on(""toolbar(tableFilter)"", function (obj) {
            switch (obj.event) {
                case ""add"":
                    add();
                    break;
            }
        });

        table.on(""tool(tableFilter)"", function (obj) {
            let data = obj.data;
            switch (obj.event) {
                case ""edit"":
                    edit(data.Id);//field Id 和 数据库表字段 Id 要一致
                    break;
                case ""del"":
                    del(data.Id);
                    break;
            }
        });
");
            WriteLiteral(@"
        function add() {
            okLayer.open(""添加用户"", ""/Donation/Donation/Add"", ""100%"", ""100%"", null, null);
        }

        function edit(id) {
            okLayer.open(""编辑用户"", ""/Donation/Donation/Edit/"" + id, ""100%"", ""100%"", null, null);
        }

        function del(id) {
            okLayer.confirm(""确定要删除吗？"", function () {
                okUtils.ajax(""/Donation/Donation/Delete"", ""get"", { id: id }, true).done(function (response) {
                    okUtils.tableSuccessMsg(response.message);
                }).fail(function (error) {
                    console.log(error)
                });
            })
        }

    })
</script>
<!-- 头工具栏模板 -->
<script type=""text/html"" id=""toolbarTpl"">
    ");
#nullable restore
#line 97 "C:\代码\Zl.WebApp\Zl.Web\Views\Donation\Donation\Index.cshtml"
Write(Html.TopToolBarHtml(ViewData["TopButtonList"]));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</script>\r\n<!-- 行工具栏模板 -->\r\n<script type=\"text/html\" id=\"operationTpl\">\r\n    ");
#nullable restore
#line 101 "C:\代码\Zl.WebApp\Zl.Web\Views\Donation\Donation\Index.cshtml"
Write(Html.RightToolBarHtml(ViewData["RightButtonList"]));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</script>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
