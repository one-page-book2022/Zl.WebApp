﻿using System;
using System.Collections.Generic;
using System.Text;
using Zl.Model;

namespace Zl.IService
{
    public interface ITaskScheduleService : IBaseService<TaskScheduleModel>
    {
        bool ResumeScheduleJob(TaskScheduleModel sm);
        bool StopScheduleJob(TaskScheduleModel sm);
    }
}
