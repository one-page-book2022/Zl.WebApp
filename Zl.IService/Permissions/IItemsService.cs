﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zl.Model;

namespace Zl.IService
{
    public interface IItemsService : IBaseService<ItemsModel>
    {
        IEnumerable<TreeSelect> GetItemsTreeSelect();
    }
}
