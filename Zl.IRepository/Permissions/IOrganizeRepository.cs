﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zl.Model;

namespace Zl.IRepository
{
    public interface IOrganizeRepository : IBaseRepository<OrganizeModel>
    {
        IEnumerable<OrganizeModel> GetOrganizeList();
    }
}
